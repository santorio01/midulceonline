
import './App.css';
import React from 'react';
import Productpage from './components/home/productPage';
import {Routes, Route, Router} from 'react-router-dom';


function App() {
  return (
    <Routes>
      <Route path='/producto' element= { <Productpage/>}/> 
    </Routes>
  )
}

export default App;
