

const listGroup = () => {
   
    return (
        <div class="row   m-2 ">
            <div class="col">
                <div class="list-group ">
                    <a href="#" class="list-group-item list-group-item-action active colorMenuList " aria-current="true">
                        The current link item
                    </a>
                    <a href="#" class="list-group-item list-group-item-action colorMenuList">A second link item</a>
                    <a href="#" class="list-group-item list-group-item-action colorMenuList">A third link item</a>
                    <a href="#" class="list-group-item list-group-item-action colorMenuList">A fourth link item</a>
                    
                </div>

            </div>
        </div>
    )


}
export default listGroup;
