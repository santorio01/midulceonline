import Logo from "./logo";
import carrito from "../img/carrito.png"
const navMenu = () => {
    return (
        <div>
            <nav class="navbar navbar-expand-lg colorMenu "  >
                <div class="container-fluid ">
                    <a class="navbar-brand" href="#"><Logo /></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link colorText " aria-current="page" href="#">Inicio</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link colorText" href="#">Productos</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link colorText" href="#">comentarios</a>
                            </li >

                        </ul>
                        <div class='row text-end col-8'>
                            <li>
                                <button><img width={50} src={carrito} /> </button>
                            </li>

                        </div>
                    </div>
                </div>
            </nav>



        </div>
    )
}
export default navMenu;