import axios from 'axios'
import { useState, useEffect } from 'react'

const CardImg = () => {
    //Declarar los objestos de los  datos
    const [productos, setProductos] = useState([])

    useEffect(() => {
        axios.get('http://localhost:9000/producto')
            .then(ress => setProductos(ress.data))
            .catch(err => console.log(err))
    }, [])

    return (

        <div class="row m-2 text-start">
            {productos.map(producto => (
                <div class="col-4  text-center " >
                    <div class="card boderColorCard colorMenu " >
                        <img height={450} src ={producto.imgProducto} class="card-img-top " />
                        <div class="card-body">
                            <h5 class="card-title">{producto.nombre}</h5>
                            <p class="card-text">{producto.descripcion}</p>
                            <a href="#" class="btn btn-primary">Añadir a carrito</a>
                        </div>
                    </div>
                </div>
            ))
            }
        </div>
    )
}

export default CardImg;