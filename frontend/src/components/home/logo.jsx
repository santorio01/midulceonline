import imgLogo from "../img/logo.png"
const logo = () =>{

    return(
        <div class="m-2 text-center col-3"  >
        <img width="120px" src={imgLogo}/>
        </div>
    )
}
export default logo;