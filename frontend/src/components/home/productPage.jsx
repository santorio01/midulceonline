import CardImg from "./cardImg";
import ListGroup from "./listGroup";
import NavMenu from "./navMenu";
import Footer from "./footer";
import React from "react";


const productPage = () =>{

    return(
        <div>
      <NavMenu/>
      <div class="row text-start">
        <div class="col-2 text-start">
      <ListGroup />
      </div>
      <div class=" m-5 col-9 text-start">
      <CardImg/>
      </div>
      </div>
      <Footer/>
    </div>
    
    )
}
export default productPage;
